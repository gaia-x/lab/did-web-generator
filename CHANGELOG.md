# [1.1.0](https://gitlab.com/gaia-x/lab/did-web-generator/compare/v1.0.1...v1.1.0) (2024-10-22)


### Bug Fixes

* npmignore ([bad361c](https://gitlab.com/gaia-x/lab/did-web-generator/commit/bad361cd9dfab41073748e5b8735cb2fc6e67ab3))
* npmignore ([9d8764a](https://gitlab.com/gaia-x/lab/did-web-generator/commit/9d8764a1768a3fd2da1bff086df1bc43c12452d3))


### Features

* add typings ([d4a786f](https://gitlab.com/gaia-x/lab/did-web-generator/commit/d4a786fcb1323dc7c7067d347163ae5b1c884c00))

# [1.1.0-development.1](https://gitlab.com/gaia-x/lab/did-web-generator/compare/v1.0.2-development.2...v1.1.0-development.1) (2024-10-22)


### Features

* add typings ([d4a786f](https://gitlab.com/gaia-x/lab/did-web-generator/commit/d4a786fcb1323dc7c7067d347163ae5b1c884c00))

## [1.0.2-development.2](https://gitlab.com/gaia-x/lab/did-web-generator/compare/v1.0.2-development.1...v1.0.2-development.2) (2024-02-05)


### Bug Fixes

* npmignore ([bad361c](https://gitlab.com/gaia-x/lab/did-web-generator/commit/bad361cd9dfab41073748e5b8735cb2fc6e67ab3))

## [1.0.2-development.1](https://gitlab.com/gaia-x/lab/did-web-generator/compare/v1.0.1...v1.0.2-development.1) (2024-02-05)


### Bug Fixes

* npmignore ([9d8764a](https://gitlab.com/gaia-x/lab/did-web-generator/commit/9d8764a1768a3fd2da1bff086df1bc43c12452d3))

## [1.0.1](https://gitlab.com/gaia-x/lab/did-web-generator/compare/v1.0.0...v1.0.1) (2024-01-25)


### Bug Fixes

* ci backmerge ([bda5d38](https://gitlab.com/gaia-x/lab/did-web-generator/commit/bda5d38c39ebea4cb75715e8cb58c111169c1c8c))

## [1.0.1-development.1](https://gitlab.com/gaia-x/lab/did-web-generator/compare/v1.0.0...v1.0.1-development.1) (2024-01-25)


### Bug Fixes

* ci backmerge ([bda5d38](https://gitlab.com/gaia-x/lab/did-web-generator/commit/bda5d38c39ebea4cb75715e8cb58c111169c1c8c))

# 1.0.0 (2024-01-24)


### Features

* doc example & ci fixup ([27e2afe](https://gitlab.com/gaia-x/lab/did-web-generator/commit/27e2afebac92ff3736a50384997bc67dc180fea9))
* first version of the did library ([9dc1e7b](https://gitlab.com/gaia-x/lab/did-web-generator/commit/9dc1e7b016b00eb511b0c41ccc168cc51457154f))

# [1.0.0-development.2](https://gitlab.com/gaia-x/lab/did-web-generator/compare/v1.0.0-development.1...v1.0.0-development.2) (2024-01-24)


### Features

* doc example & ci fixup ([27e2afe](https://gitlab.com/gaia-x/lab/did-web-generator/commit/27e2afebac92ff3736a50384997bc67dc180fea9))

# 1.0.0-development.1 (2024-01-24)


### Features

* first version of the did library ([9dc1e7b](https://gitlab.com/gaia-x/lab/did-web-generator/commit/9dc1e7b016b00eb511b0c41ccc168cc51457154f))
