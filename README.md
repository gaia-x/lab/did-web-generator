# Gaia-X AISBL DID Generator Library

This library allows you to generate a ready to use [DID](https://www.w3.org/TR/did-core/).

It uses your certificate to generate it, and thus relies on several x509/crypto libraries to work.

## Usage


```typescript
import {createDidDocument} from '@gaia-x/did-web-generator'
//...
function getDid(){
    return createDidDocument("https://mycompanydomain.com", "x509Certificate.pem", myX509Cert)
}
```

The `myX509Cert` variable needs to be an X.509 certificate string, you can check out 
[`src/tests/did-generator.spec.ts`](src/tests/did-generator.spec.ts) for an example value.

will return a json object looking like this
```json
{
  "@context": [
    "https://www.w3.org/ns/did/v1",
    "https://w3id.org/security/suites/jws-2020/v1"
  ],
  "id": "did:web:mycompanydomain.com",
  "verificationMethod": [
    {
      "@context": "https://w3c-ccg.github.io/lds-jws2020/contexts/v1/",
      "id": "did:web:mycompanydomain.com#X509-JWK2020",
      "type": "JsonWebKey2020",
      "controller": "did:web:mycompanydomain.com",
      "publicKeyJwk": {
        "kty": "RSA",
        "n": "...",
        "e": "AQAB",
        "alg": "RS384",
        "x5u": "http://mycompanydomain.com/.well-known/x509Certificate.pem"
      }
    }
  ],
  "assertionMethod": [
    "did:web:mycompanydomain.com#X509-JWK2020"
  ]
}
```

The DID creation process requires certificate analysis, we suggest you cache the result DID to avoid more computation 
