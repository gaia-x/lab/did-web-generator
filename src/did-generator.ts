import * as jose from 'jose'
import * as jsrsasign from 'jsrsasign'
import { JCAMapping } from './jca-mappings'
import type { DID } from './did'

const X509_VERIFICATION_METHOD_NAME = 'X509-JWK2020'

/**
 * Returns a formatted string representing in did:web the base url passed
 * @param componentBaseURL the base URL the did will be accessed on
 * @example `https://mydomain.com` will become `did:web:mydomain.com`
 */
export function getDidWeb (componentBaseURL: string): string {
  return `did:web:${componentBaseURL
    .replace(/http[s]?:\/\//, '')
    .replace(':', '%3A') // encode port ':' as '%3A' in did:web
    .replace(/\//g, ':')}`
}

/**
 * This generates a URI based on the certificate file name and the base URL of the component
 * @example componentBaseURL=https://mydomain.com certificateFileName=mycert.pem returns https://mydomain.com/.well-known/mycert.pem
 */
export function getCertChainUri (
  componentBaseURL: string,
  certificateFileName: string
): string {
  return `${componentBaseURL}/.well-known/${certificateFileName}`
}

export function getJCAMappingForAlgorithm (signatureAlgorithm: string): string {
  if (JCAMapping.has(signatureAlgorithm)) {
    return JCAMapping.get(signatureAlgorithm)?.JWS ?? 'PS256'
  }
  return 'PS256'
}

export async function createDidDocument (
  componentBaseURL: string,
  certificateFileName: string,
  x509Certificate: string
): Promise<DID> {
  const cert = new jsrsasign.X509()
  cert.readCertPEM(x509Certificate)

  const didWeb = getDidWeb(componentBaseURL)

  const spki = await jose.importX509(
    x509Certificate,
    getJCAMappingForAlgorithm(cert.getSignatureAlgorithmName())
  )
  const x509VerificationMethodIdentifier = `${didWeb}#${X509_VERIFICATION_METHOD_NAME}`

  return {
    '@context': [
      'https://www.w3.org/ns/did/v1',
      'https://w3id.org/security/suites/jws-2020/v1'
    ],
    id: didWeb,
    verificationMethod: [
      {
        '@context': 'https://w3c-ccg.github.io/lds-jws2020/contexts/v1/',
        id: x509VerificationMethodIdentifier,
        type: 'JsonWebKey2020',
        controller: didWeb,
        publicKeyJwk: {
          ...(await jose.exportJWK(spki)),
          alg: getJCAMappingForAlgorithm(cert.getSignatureAlgorithmName()),
          x5u: getCertChainUri(componentBaseURL, certificateFileName)
        }
      }
    ],
    assertionMethod: [x509VerificationMethodIdentifier]
  }
}
