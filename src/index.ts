import { getDidWeb, getCertChainUri, createDidDocument } from './did-generator'
import { JCAMapping } from './jca-mappings'

export { getDidWeb, getCertChainUri, createDidDocument, JCAMapping }
