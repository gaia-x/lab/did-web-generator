import {
  createDidDocument,
  getCertChainUri,
  getDidWeb
} from '../did-generator'
import { describe, expect, it } from 'vitest'

const x509String = `-----BEGIN CERTIFICATE-----
MIIDoTCCAomgAwIBAgIUZdOt6VdIxDPgaPmHiD2IviYuIPowDQYJKoZIhvcNAQEL
BQAwXzELMAkGA1UEBhMCQkUxETAPBgNVBAgMCEJydXNzZWxzMREwDwYDVQQHDAhC
cnVzc2VsczEUMBIGA1UECgwLR2FpYS1YIFRlc3QxFDASBgNVBAMMC0dhaWEtWCBU
ZXN0MCAXDTI0MDExNzE1MjEyOFoYDzIxMjMxMjI0MTUyMTI4WjBfMQswCQYDVQQG
EwJCRTERMA8GA1UECAwIQnJ1c3NlbHMxETAPBgNVBAcMCEJydXNzZWxzMRQwEgYD
VQQKDAtHYWlhLVggVGVzdDEUMBIGA1UEAwwLR2FpYS1YIFRlc3QwggEiMA0GCSqG
SIb3DQEBAQUAA4IBDwAwggEKAoIBAQC20ogh9CLYlnSlDDqEZvP4A24zN7pIlYQ1
lNyw2Vd5yqyLilXnR1tSKiyPFPSC0PfMPL8jW8OShWJPj7ntABzunIHxwcySrvpT
i6FjiCnBV7e9pWZXUGt5cK1GmoJGtt4ZiAqlhFrMi+wqjlImLgypsiKdRu7myG+h
KToDvmAlRLfYmnX9U+PxIG/w0VnaLCgp5sCV0yiplZl4344ZKe2EI/8roYR9l9YR
WySE60XqSpoggOCtcXPSS6U02qg0DUQmi6UoG6el9Q/2GOd12dSXtMA4pOKPnHF1
dHfbjbcsBafAPCyJfnulJjdOWxcn4yN2b7yhRfODczgqLPkburkXAgMBAAGjUzBR
MB0GA1UdDgQWBBRhuYg20UduO/iEu5/pUVoeQWmUUDAfBgNVHSMEGDAWgBRhuYg2
0UduO/iEu5/pUVoeQWmUUDAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUA
A4IBAQAcgvL0jVoL8ngAVkNW40gybLUUGLOWWXRDeTYjGm3F1HaETbfwhfCdyzqa
V3Fp+d5tK+4++shyO+u5bQh5peG+Wl0HGNkLGFdl6hhvWLAZt4PtyL8FdWE4wt0R
zW3XAuXs08bZDXn3Ao6FEQA7cz58zZa9bwI6iTs3yH34q6LLaAUHbcuxQ2C9btPU
icSEjEege3AxxO+/W0vSvWZkZFX4Wfv5RSxA5OL5dv8nDSSXjBcP8BIgPMjc45vB
QhvFPGLlzUyeMXW3Ul8Mj0+TuBU7qNggtTLdAsaL7tt8wkunBvry9+HgGarU40U8
85uoL5Mz2kubFAz44yjC7X7jTNq8
-----END CERTIFICATE-----`

describe('did.utils', () => {
  it('should return a did composed of the component base url even containing several slashes', () => {
    // Given
    const baseURL = 'https://domain.com/folder/subfolder'
    // When
    const didWeb = getDidWeb(baseURL)
    // Then
    expect(didWeb).toEqual('did:web:domain.com:folder:subfolder')
  })
  it('should return a did composed of the component base url', () => {
    // Given
    const baseURL = 'https://domain.com'
    // When
    const didWeb = getDidWeb(baseURL)
    // Then
    expect(didWeb).toEqual('did:web:domain.com')
  })
  it('should return a certificate url using component base url', () => {
    // Given
    const baseURL = 'https://mydomain.com'
    const certificateName = 'x509CertificateChain.pem'
    // When
    const certUrl = getCertChainUri(baseURL, certificateName)
    // Then
    expect(certUrl).toEqual(
      'https://mydomain.com/.well-known/x509CertificateChain.pem'
    )
  })
  it('should generate a did based on public key and component base url', async () => {
    // Given
    const baseURL = 'https://asuperdomain.com'
    const certificateName = 'x509CertificateChain.pem'

    // When
    const did = await createDidDocument(baseURL, certificateName, x509String)
    // Then
    expect(did).toBeDefined()
    expect(did.id).toEqual('did:web:asuperdomain.com')
    expect(did.verificationMethod[0].controller).toEqual(
      'did:web:asuperdomain.com'
    )
    expect(did.verificationMethod[0].id).toEqual(
      'did:web:asuperdomain.com#X509-JWK2020'
    )
    expect(did.verificationMethod[0].publicKeyJwk.alg).toEqual('RS256')
    expect(did.verificationMethod[0].publicKeyJwk.n).toEqual(
      'ttKIIfQi2JZ0pQw6hGbz-ANuMze6SJWENZTcsNlXecqsi4pV50dbUiosjxT0gtD3zDy_I1vDkoViT4-57QAc7pyB8cHMkq76U4uhY4gpwVe3vaVmV1BreXCtRpqCRrbeGYgKpYRazIvsKo5SJi4MqbIinUbu5shvoSk6A75gJUS32Jp1_VPj8SBv8NFZ2iwoKebAldMoqZWZeN-OGSnthCP_K6GEfZfWEVskhOtF6kqaIIDgrXFz0kulNNqoNA1EJoulKBunpfUP9hjnddnUl7TAOKTij5xxdXR32423LAWnwDwsiX57pSY3TlsXJ-Mjdm-8oUXzg3M4Kiz5G7q5Fw'
    )
    expect(did.verificationMethod[0].publicKeyJwk.kty).toEqual('RSA')
  })
})
