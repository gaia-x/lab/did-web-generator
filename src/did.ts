export interface DID {
  '@context': object | object[] | string[] | string
  id: string
  verificationMethod: VerificationMethod[]
  assertionMethod: string[]
}

export interface VerificationMethod {
  '@context': object | object[] | string[] | string
  id: object | string
  type: string
  controller: string
  publicKeyJwk: PublicJWK
}

export interface PublicJWK {
  alg?: string
  n?: string
  kty?: string
  x5u?: string
}
